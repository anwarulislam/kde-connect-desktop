# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2015, 2016, 2017, 2018, 2019, 2020.
# Vit Pelcak <vit@pelcak.org>, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-22 00:41+0000\n"
"PO-Revision-Date: 2023-06-23 09:37+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.2\n"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1% charging"
msgstr "%1% nabíjí se"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "No info"
msgstr "Žádné informace"

#: package/contents/ui/Connectivity.qml:40
#, kde-format
msgid "Unknown"
msgstr "Neznámý"

#: package/contents/ui/Connectivity.qml:50
#, kde-format
msgid "No signal"
msgstr "Žádný signál"

#: package/contents/ui/DeviceDelegate.qml:56
#, kde-format
msgid "File Transfer"
msgstr "Přenos souboru"

#: package/contents/ui/DeviceDelegate.qml:57
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "Zde upusťte soubor pro jeho přesun do vašeho telefonu."

# 56606 AttribValues/label
#: package/contents/ui/DeviceDelegate.qml:93
#, kde-format
msgid "Virtual Display"
msgstr "Virtuální obrazovka"

#: package/contents/ui/DeviceDelegate.qml:146
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:168
#, kde-format
msgid "Please choose a file"
msgstr "´Prosím, vyberte soubor"

#: package/contents/ui/DeviceDelegate.qml:177
#, kde-format
msgid "Share file"
msgstr "Sdílet soubor"

#: package/contents/ui/DeviceDelegate.qml:192
#, kde-format
msgid "Send Clipboard"
msgstr "Poslat schránku"

#: package/contents/ui/DeviceDelegate.qml:211
#, kde-format
msgid "Ring my phone"
msgstr "Prozvonit můj telefon"

#: package/contents/ui/DeviceDelegate.qml:229
#, kde-format
msgid "Browse this device"
msgstr "Procházet toto zařízení"

#: package/contents/ui/DeviceDelegate.qml:246
#, kde-format
msgid "SMS Messages"
msgstr "Zprávy SMS"

#: package/contents/ui/DeviceDelegate.qml:267
#, kde-format
msgid "Remote Keyboard"
msgstr "Vzdálená klávesnice"

#: package/contents/ui/DeviceDelegate.qml:288
#, kde-format
msgid "Notifications:"
msgstr "Oznámení:"

#: package/contents/ui/DeviceDelegate.qml:296
#, kde-format
msgid "Dismiss all notifications"
msgstr "Zahodit všechna oznámení"

#: package/contents/ui/DeviceDelegate.qml:343
#, kde-format
msgid "Reply"
msgstr "Odpovědět"

#: package/contents/ui/DeviceDelegate.qml:353
#, kde-format
msgid "Dismiss"
msgstr "Zahodit"

#: package/contents/ui/DeviceDelegate.qml:366
#, kde-format
msgid "Cancel"
msgstr "Zrušit"

#: package/contents/ui/DeviceDelegate.qml:380
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr "Odpovědět %1…"

#: package/contents/ui/DeviceDelegate.qml:398
#, kde-format
msgid "Send"
msgstr "Odeslat"

#: package/contents/ui/DeviceDelegate.qml:424
#, kde-format
msgid "Run command"
msgstr "Spustit příkaz:"

#: package/contents/ui/DeviceDelegate.qml:432
#, kde-format
msgid "Add command"
msgstr "Přidat příkaz"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "No paired devices"
msgstr "Žádná spárovaná zařízení"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "Spárované zařízení není dostupné"
msgstr[1] "Všechna spárovaná zařízení jsou nedostupná"
msgstr[2] "Všechna spárovaná zařízení jsou nedostupná"

#: package/contents/ui/FullRepresentation.qml:60
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr ""
"Pro integraci zařízení Android a prostředí Plasma je potřeba nainstalovat "
"KDE Connect."

#: package/contents/ui/FullRepresentation.qml:64
#, kde-format
msgid "Pair a Device..."
msgstr "Spárovat zařízení..."

#: package/contents/ui/FullRepresentation.qml:76
#, kde-format
msgid "Install from Google Play"
msgstr "Instalovat z Google Play"

#: package/contents/ui/FullRepresentation.qml:86
#, kde-format
msgid "Install from F-Droid"
msgstr "Instalovat z F-Droidu"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "KDE Connect Settings..."
msgstr "Nastavení KDE Connect..."
